Drone Dgraph Demo

This is based off the [Dgraph-js](https://github.com/dgraph-io/dgraph-js) simple example.

It shows how to run Node.js Jest tests with Dgraph as a service in the Drone CI pipeline.

