const dgraph = require("dgraph-js");
const grpc = require("grpc");
const utils = require("./utils");

async function main() {
    const dgraphClientStub = utils.newClientStub();
    const dgraphClient = utils.newClient(dgraphClientStub);
    await utils.dropAll(dgraphClient);
    await utils.setSchema(dgraphClient);
    await utils.createData(dgraphClient);
    await utils.queryData(dgraphClient, "Alice");

    // Close the client stub.
    dgraphClientStub.close();
}

main().then(() => {
    console.log("\nDONE!");
}).catch((e) => {
    console.log("ERROR: ", e);
});
