const utils = require("./utils");

describe("Demo Dgraph", () => {
  let dgraphClient;

  beforeAll(async (done) => {
    const dgraphClientStub = utils.newClientStub();
    dgraphClient = utils.newClient(dgraphClientStub);
    await utils.dropAll(dgraphClient);
    await utils.setSchema(dgraphClient);
    await utils.createData(dgraphClient);
    done();
  });

  test(`Returns a person named Alice`, async (done) => {
    const result = await utils.queryData(dgraphClient, "Alice");
    expect(result.all.length)
      .toEqual(1);
    expect (result.all[0].name)
      .toEqual("Alice");
    done();
  })

  test(`Returns a person named Bob`, async (done) => {
    const result = await utils.queryData(dgraphClient, "Bob");
    expect(result.all.length)
      .toEqual(1);
    expect (result.all[0].name)
      .toEqual("Bob");
    done();
  })
});
